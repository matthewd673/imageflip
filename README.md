# README #

## What does it do?
It flips images horizontally and vertically and outputs them to the Form

## How does it work?
It manually reads and sets each pixel's color from the source to the output.
The code is pretty well commented, so it should be easy to port to other languages

## How do I run it?
You can run the program by downloading the repo and opening it in Visual Studio 2017.
There should also be an .exe in the bin/Debug folder you can use.

**NOTE:**
The default filepath is a file on my computer, you'll want to change the default or input your own.