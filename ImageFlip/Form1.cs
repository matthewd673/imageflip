﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ImageFlip
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        //the four images
        Bitmap source;
        Bitmap horizontal;
        Bitmap vertical;
        Bitmap both;

        //this was just my test image path, use your own
        string filepath = "C:\\Users\\Matthew\\Pictures\\smolchore.png";

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            //you can change the image, or just use
            //the one defined above
            if(textBox1.Text != "filepath")
            {
                filepath = textBox1.Text;
            }

            //define the variables
            //note how horizontal, vertical, and both are all
            //left blank but still are the same size as source
            source = new Bitmap(filepath);
            horizontal = new Bitmap(source.Width, source.Height);
            vertical = new Bitmap(source.Width, source.Height);
            both = new Bitmap(source.Width, source.Height);
            flip();
            panel1.Refresh();
        }

        public void flip()
        {
            //just a quick protection
            if(source != null)
            {
                //cycle through each pixel in the source image
                for(int i = 0; i < source.Width; i++)
                {
                    for(int j = 0; j < source.Height; j++)
                    {
                        //find the current color of the pixel in the source
                        Color currentColor;
                        currentColor = source.GetPixel(i, j);
                        //set the opposite pixel that same color
                        //note that -1, only use that for the manipulated value (ie: width)
                        //i forget why thats needed, it has something to do with the
                        //subtraction
                        horizontal.SetPixel(horizontal.Width - i - 1, j, currentColor);
                        vertical.SetPixel(i, vertical.Height - j - 1, currentColor);
                        both.SetPixel(both.Width - i - 1, both.Height - j - 1, currentColor);
                    }
                }
            }
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {
            //this is just so i can render it out
            //you could very easily just save the resulting images
            Graphics g = panel1.CreateGraphics();

            //some more protections
            if (source != null && horizontal != null && vertical != null && both != null)
            {
                //just renders the images side by side
                g.DrawImage(source, new Point(0, 0));
                g.DrawImage(horizontal, new Point(source.Width, 0));
                g.DrawImage(vertical, new Point(0, source.Height));
                g.DrawImage(both, new Point(source.Width, source.Height));
            }
        }
    }
}
